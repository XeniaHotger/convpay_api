ConvPay API
=========================

Если возникают проблемы с доступностью на запись папкок var/cache и var/logs:
```
sudo chmod -R 777 symfony/var/cache/ 
sudo chmod -R 777 symfony/var/logs/
sudo chmod -R 777 symfony/var/sessions/
```

# Webpack
Запуск в режиме watch: `docker-compose exec php npm run watch`
Запуск в режиме однократной сборки: `docker-compose exec php npm run build`

# DB
Локальная база: `docker-compose exec db mysql -uroot -pdasopdjsailaspf convpayapi`
Продовская база: `docker-compose exec db mysql -uroot -pWpFv5DCZqp9aBcYX convpayapi`

# Deploy

1) Залить на сервер
2) docker-compose -f docker-compose.prod.yml build - сборка
3) docker-compose -f docker-compose.prod.yml up - запуск

#Обновление базы подписок
1) Зайти на сервер
2) Залогиниться в базу командой `docker-compose exec db mysql -u -p convpayapi`
3) Команда вставки записи `INSERT INTO subscription_info (sub_id, sub_name, sub_duration) VALUES(521935, "Flvto YouTube Converter 1 Month Subscription", 31);` // 31 - длительность подписки в днях
4) Команда обновления существующей записи `UPDATE subscription_info SET sub_name = "This is a new title" WHERE sub_id = 521935;`

#База данных
ToDo: необходимо настроить резервное копирование.
А пока нужно крайне аккуратно работать с базой, потому что, если что-то пойдет не так, все данные о подписках могут быть утеряны.