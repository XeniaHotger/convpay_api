<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171212205026 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE subscriptions_info (id INT AUTO_INCREMENT NOT NULL, SubId VARCHAR(15) NOT NULL, SubName VARCHAR(255) NOT NULL, SubDuration INT NOT NULL, UNIQUE INDEX UNIQ_1CFA8572C5475CEB (SubId), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE paddle_subscriptions (id INT AUTO_INCREMENT NOT NULL, LicenseId VARCHAR(255) NOT NULL, SubId VARCHAR(15) NOT NULL, PaymentDateTime DATETIME NOT NULL, ExpirationDateTime DATETIME NOT NULL, UNIQUE INDEX UNIQ_5B1A389899C0924 (LicenseId), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE license_users (id INT AUTO_INCREMENT NOT NULL, SubId VARCHAR(255) NOT NULL, LicenseId VARCHAR(255) NOT NULL, DeviceGuid VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE subscriptions_info');
        $this->addSql('DROP TABLE paddle_subscriptions');
        $this->addSql('DROP TABLE license_users');
    }
}
