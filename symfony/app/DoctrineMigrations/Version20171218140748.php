<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171218140748 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE subscription_info (id INT AUTO_INCREMENT NOT NULL, sub_id VARCHAR(15) NOT NULL, sub_name VARCHAR(255) NOT NULL, sub_duration INT NOT NULL, UNIQUE INDEX UNIQ_FCB5214956992D9 (sub_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE paddle_subscription (id INT AUTO_INCREMENT NOT NULL, subscription_id INT NOT NULL, license_id VARCHAR(255) NOT NULL, payment_date_time DATETIME NOT NULL, expiration_date_time DATETIME NOT NULL, UNIQUE INDEX UNIQ_BE9EEA92460F904B (license_id), INDEX IDX_BE9EEA929A1887DC (subscription_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE license_user (id INT AUTO_INCREMENT NOT NULL, paddle_subscription_id INT NOT NULL, license_id VARCHAR(255) NOT NULL, device_guid VARCHAR(255) NOT NULL, INDEX IDX_77C835A2BF00C35 (paddle_subscription_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE paddle_subscription ADD CONSTRAINT FK_BE9EEA929A1887DC FOREIGN KEY (subscription_id) REFERENCES subscription_info (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE license_user ADD CONSTRAINT FK_77C835A2BF00C35 FOREIGN KEY (paddle_subscription_id) REFERENCES paddle_subscription (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE license_users');
        $this->addSql('DROP TABLE paddle_subscriptions');
        $this->addSql('DROP TABLE subscriptions_info');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE paddle_subscription DROP FOREIGN KEY FK_BE9EEA929A1887DC');
        $this->addSql('ALTER TABLE license_user DROP FOREIGN KEY FK_77C835A2BF00C35');
        $this->addSql('CREATE TABLE license_users (id INT AUTO_INCREMENT NOT NULL, SubId VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, LicenseId VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DeviceGuid VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE paddle_subscriptions (id INT AUTO_INCREMENT NOT NULL, LicenseId VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, SubId VARCHAR(15) NOT NULL COLLATE utf8_unicode_ci, PaymentDateTime DATETIME NOT NULL, ExpirationDateTime DATETIME NOT NULL, UNIQUE INDEX UNIQ_5B1A389899C0924 (LicenseId), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subscriptions_info (id INT AUTO_INCREMENT NOT NULL, SubId VARCHAR(15) NOT NULL COLLATE utf8_unicode_ci, SubName VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, SubDuration INT NOT NULL, UNIQUE INDEX UNIQ_1CFA8572C5475CEB (SubId), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE subscription_info');
        $this->addSql('DROP TABLE paddle_subscription');
        $this->addSql('DROP TABLE license_user');
    }
}
