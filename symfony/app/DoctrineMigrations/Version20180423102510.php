<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180423102510 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE plan_subscription (id INT AUTO_INCREMENT NOT NULL, plan_id INT NOT NULL, checkout_id VARCHAR(255) NOT NULL, subscription_id VARCHAR(255) DEFAULT NULL, device_guid VARCHAR(255) NOT NULL, payment_date_time DATETIME NOT NULL, expiration_date_time DATETIME NOT NULL, next_bill_date_time DATETIME DEFAULT NULL, update_url VARCHAR(1020) DEFAULT NULL, cancel_url VARCHAR(1020) DEFAULT NULL, UNIQUE INDEX UNIQ_795265DB146D8724 (checkout_id), UNIQUE INDEX UNIQ_795265DB9A1887DC (subscription_id), UNIQUE INDEX plan_device_unique (plan_id, device_guid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE plan_subscription');
    }
}
