<?php

namespace AppBundle\Controller\Paddle;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Repository\PaddleSubscriptionRepository;
use AppBundle\Repository\LicenseUserRepository;
use AppBundle\Repository\SubscriptionInfoRepository;
use AppBundle\Service\PaddleSubscription as PaddleSubService;

/**
 * Class Controller
 * @Route("/paddle")
 */
class Controller extends \Symfony\Bundle\FrameworkBundle\Controller\Controller
{
    /**
     * Активна ли подписка данного юзера на данный продукт
     *
     * @Route("/is-active-product")
     * @Method("GET")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function activeProductAction(Request $request)
    {
        if ($request->get('device_guid') === null || $request->get('sub_id') === null) {
            return new JsonResponse(['isActive' => false]);
        }

        /** @var SubscriptionInfoRepository $subscriptionInfoRep */
        $subscriptionInfoRep = $this->getDoctrine()->getRepository('AppBundle:SubscriptionInfo');

        /** @var PaddleSubscriptionRepository $subscriptionRep */
        $subscriptionRep = $this->getDoctrine()->getRepository('AppBundle:PaddleSubscription');
        $activeSub = $subscriptionRep->getSubByUserAndProduct(
            $request->get('device_guid'),
            $subscriptionInfoRep->getSubKeyById($request->get('sub_id'))
        );

        return new JsonResponse(['isActive' => $activeSub && $activeSub->getExpirationDateTime() > (new \DateTime())]);
    }

    /**
     * Активна ли подписка данного юзера
     *
     * @Route("/is-active-user")
     * @Method("GET")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function activeUserAction(Request $request)
    {
        if ($request->get('device_guid') === null) {
            return new JsonResponse(['isActive' => false]);
        }

        // get all licenses of the user
        /** @var LicenseUserRepository $licenseRep */
        $licenseRep = $this->getDoctrine()->getRepository('AppBundle:LicenseUser');
        $activeLicenses = $licenseRep->getLicensesByDevice($request->get('device_guid'));

        // if the user has no licenses
        if (count($activeLicenses) == 0) {
            return new JsonResponse(['isActive' => false]);
        }

        // get all subs by the licenses
        /** @var PaddleSubscriptionRepository $subscriptionRep */
        $subscriptionRep = $this->getDoctrine()->getRepository('AppBundle:PaddleSubscription');

        $currentDateTime = new \DateTime();
        // if there is at least 1 active license, return true
        foreach ($subscriptionRep->getSubsByLicenseIds($activeLicenses) as $sub) {
            if ($sub->getExpirationDateTime() > $currentDateTime) {
                return new JsonResponse(['isActive' => true]);
            }
        }

        // if there is no active subs
        return new JsonResponse(['isActive' => false]);
    }

    /**
     * Активен ли данный ключ лицензии. (если не достигнут лимит, и данный юзер не связан с данным ключом, то связываем)
     *
     * @Route("/is-active-license")
     * @Method("GET")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function licenseInfoAction(Request $request)
    {
        if ($request->get('license_id') === null || $request->get('device_guid') === null) {
            return new JsonResponse(['isActive' => false]);
        }

        $licenseId = $request->get('license_id');

        /** @var PaddleSubscriptionRepository $subscriptionRep */
        $subscriptionRep = $this->getDoctrine()->getRepository('AppBundle:PaddleSubscription');
        $activeSub = $subscriptionRep->getSubByLicenseId($licenseId);

        $isActiveSubscription = false;
        if ($activeSub !== null) {
            /** @var PaddleSubService $paddleSubscriptionServ */
            $paddleSubscriptionServ = $this->get('subscription');
            if (!$paddleSubscriptionServ->addDeviceGuidIfNotExisted($request->get('device_guid'), $licenseId, $activeSub)) {
                return new JsonResponse(['isActive' => false]);
            }

            $isActiveSubscription = $activeSub->getExpirationDateTime() > (new \DateTime('now'));
        }

        return new JsonResponse(['isActive' => $isActiveSubscription]);
    }

    /**
     * Добавление в базу новой записи о подписке юзера или ее продление при успешном завершении оплаты [wrapping]
     *
     * @Route("/add-paid-sub")
     * @Method("POST")
     * @param Request $request
     *
     * @return Response
     *
     * @deprecated
     */
    public function newSubscribeAction(Request $request)
    {
        if (
            $request->get('license_id') === null ||
            $request->get('device_guid') === null ||
            $request->get('sub_id') === null
        ) {
            return new JsonResponse(['status' => false]);
        }

        try {
            /** @var PaddleSubService $paddleSubscriptionServ */
            $paddleSubscriptionServ = $this->get('subscription');
            $subscriptionStatus = $paddleSubscriptionServ->addToDatabase(
                $request->get('license_id'),
                $request->get('device_guid'),
                $request->get('sub_id'),
                new \DateTime('now')
            );
        }
        catch(\Exception $e) {
            $subscriptionStatus = false;
        }

        return new JsonResponse(['status' => $subscriptionStatus]);
    }

    /**
     * Оплата продукта с помощью инструмента сервиса Paddle
     *
     * @Route("/pay")
     * @Method("GET")
     * @param Request $request
     *
     * @return Response
     */
    public function payAction(Request $request)
    {
        $subId = $request->get('sub_id');
        $deviceGuid = $request->get('device_guid');
        $errorMessage = '';

        if ($deviceGuid === null || $subId === null) {
            $subId = 0;
            $deviceGuid = '';
            $errorMessage = 'Empty parameter(s) were passed';
        }

        return $this->render('AppBundle:paddle:pay.html.twig', [
            'subId' => $subId,
            'deviceGuid' => $deviceGuid,
            'errorMessage' => $errorMessage,
        ]);
    }
}
