<?php

namespace AppBundle\Controller\Paddle;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\PaddleSubscription;
use AppBundle\Repository\SubscriptionInfoRepository;
use AppBundle\Service\PaddleSubscription as PaddleSubService;
use AppBundle\Service\PaddleApiRequest;

/**
 * Class V2Controller
 * @Route("/paddle/v2")
 */
class V2Controller extends Controller
{
    /**
     * Добавление в базу новой записи о подписке юзера или ее продление
     *
     * @param Request $request
     *
     * @return integer
     */
    protected function paymentSuccess(Request $request)
    {
        if ($request->get('checkout_id') === null || $request->get('passthrough') === null) {
            return 404;
        }
        $orderInfo = $this->getOrderInfoAction($request->get('checkout_id'));

        if (!is_array($orderInfo) || !isset($orderInfo['lockers'])) {
            return 404;
        }
        $currentInfo = current($orderInfo['lockers']);

        if (is_array($currentInfo) && $currentInfo['license_code'] && $currentInfo['product_id']) {
            $licenseId = $currentInfo['license_code'];
            $deviceGuid = $request->get('passthrough');
            $subId = $currentInfo['product_id'];

            /** @var SubscriptionInfoRepository $subscriptionInfoRep */
            $subscriptionInfoRep = $this->getDoctrine()->getRepository('AppBundle:SubscriptionInfo');
            $subDuration = $subscriptionInfoRep->getSubDuration($subId);
            try {
                /** @var PaddleSubService $paddleSubscriptionServ */
                $paddleSubscriptionServ = $this->get('subscription');
                $currentSub = $paddleSubscriptionServ->getCurrentSub($deviceGuid, $subId);
                $expirationDateTime = (new \DateTime('now'))->modify("+$subDuration day")
                    ->modify('-1 hour');

                if ($currentSub instanceof PaddleSubscription) {
                    $oldExpirationDateTime = $currentSub->getExpirationDateTime();
                    if ($oldExpirationDateTime > new \DateTime('now')) {
                        $expirationDateTime = $oldExpirationDateTime->modify("+$subDuration day")
                            ->modify('-1 hour');
                    }
                    $status = $paddleSubscriptionServ->updateDatabase($licenseId, $deviceGuid, $currentSub, $expirationDateTime);
                } else {
                    $status = $paddleSubscriptionServ->addToDatabase($licenseId, $deviceGuid, $subId, $expirationDateTime);
                }
            } catch (\Exception $e) {
                $status = false;
            }

            return $status ? 200 : 500;
        }

        return 404;
    }

    /**
     * Добавление в базу новой записи о подписке юзера или ее продление (на час, в ожидании вебхука подтверждения оплаты)
     *
     * @Route("/payment-init")
     * @Method("POST")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function paymentInitAction(Request $request)
    {
        if (
            $request->get('license_id') === null ||
            $request->get('device_guid') === null ||
            $request->get('sub_id') === null
        ) {
            return new JsonResponse(['status' => false]);
        }

        $licenseId = $request->get('license_id');
        $deviceGuid = $request->get('device_guid');
        $subId = $request->get('sub_id');
        try {
            /** @var PaddleSubService $paddleSubscriptionServ */
            $paddleSubscriptionServ = $this->get('subscription');
            $currentSub = $paddleSubscriptionServ->getCurrentSub($deviceGuid, $subId);
            $expirationDateTime = (new \DateTime('now'))->modify('+1 hour');

            if ($currentSub instanceof PaddleSubscription) {
                $oldExpirationDateTime = $currentSub->getExpirationDateTime();
                if ($oldExpirationDateTime > new \DateTime('now')) {
                    $expirationDateTime = $oldExpirationDateTime->modify('+1 hour');
                }
                $status = $paddleSubscriptionServ->updateDatabase($licenseId, $deviceGuid, $currentSub, $expirationDateTime);
            } else {
                $status = $paddleSubscriptionServ->addToDatabase($licenseId, $deviceGuid, $subId, $expirationDateTime);
            }

            return new JsonResponse(['status' => $status]);
        }
        catch(\Exception $e) {
            return new JsonResponse(['status' => false]);
        }
    }

    /**
     * Добавление в базу новой записи о подписке юзера или ее продление (вызывается Paddle-ом)
     * Webhook Alert "Payment Success (Subscription)" (подробнее - paddle.com/docs/reference-using-webhooks)
     *
     * @Route("/payment-success")
     * @Method("POST")
     * @param Request $request
     *
     * @return Response
     */
    public function paymentSuccessAction(Request $request)
    {
        return (new Response())->setStatusCode($this->paymentSuccess($request));
    }

    /**
     * Получение информации по заданной оплате
     *
     * @param string $checkoutId
     *
     * @return array | bool
     */
    public function getOrderInfoAction($checkoutId)
    {
        $options = [
            CURLOPT_URL => 'http://checkout.paddle.com/api/1.0/order?checkout_id=' . $checkoutId,
        ];
        /** @var PaddleApiRequest $apiRequestServ */
        $apiRequestServ = $this->get('paddle_api_request');

        return $apiRequestServ->doCurlRequest($options);
    }
}