<?php

namespace AppBundle\Controller\Paddle;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Repository\PlanSubscriptionRepository;
use AppBundle\Entity\PlanSubscription;
use AppBundle\Service\PlanSubscription as PlanService;
use AppBundle\Service\PaddleApiRequest;

/**
 * Class V3Controller
 * @Route("/paddle/v3")
 */
class V3Controller extends V2Controller
{
    /**
     * Оплата подписки с помощью инструмента сервиса Paddle
     *
     * @Route("/subscribe")
     * @Method("GET")
     * @param Request $request
     *
     * @return Response
     */
    public function subscribeAction(Request $request)
    {
        $planId = $request->get('plan_id');
        $deviceGuid = $request->get('device_guid');
        $errorMessage = '';

        if ($deviceGuid === null || $planId === null) {
            $planId = 0;
            $deviceGuid = '';
            $errorMessage = 'Empty parameter(s) were passed';
        }

        return $this->render('AppBundle:paddle:subscribe.html.twig', [
            'planId' => $planId,
            'deviceGuid' => $deviceGuid,
            'errorMessage' => $errorMessage,
        ]);
    }

    /**
     * Есть ли у данного юзера активная подписка
     *
     * @Route("/is-active-subscription-user")
     * @Method("GET")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function activeSubscriptionUserAction(Request $request)
    {
        if ($request->get('device_guid') === null) {
            return new JsonResponse(['isActive' => false]);
        }

        /** @var PlanSubscriptionRepository $subscriptionRep */
        $subscriptionRep = $this->getDoctrine()->getRepository('AppBundle:PlanSubscription');
        $activeSub = $subscriptionRep->getSubByUser($request->get('device_guid'));

        return new JsonResponse([
            'isActive' => $activeSub instanceof PlanSubscription &&
                $activeSub->getExpirationDateTime() > (new \DateTime())
        ]);
    }

    /**
     * Возвращает ссылку, по которой должен перейти юзер для смены способа оплаты подписки
     *
     * @Route("/get-update-payment-method-url")
     * @Method("GET")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getUpdatePaymentMethodUrl(Request $request)
    {
        if ($request->get('device_guid') === null) {
            return new JsonResponse(['status' => 'error']);
        }

        /** @var PlanSubscriptionRepository $subscriptionRep */
        $subscriptionRep = $this->getDoctrine()->getRepository('AppBundle:PlanSubscription');
        $updateUrl = $subscriptionRep->getSubByUser($request->get('device_guid'))->getUpdateUrl();

        return new JsonResponse(
            $updateUrl ?
                ['status' => 'success', 'url' => $updateUrl] :
                ['status' => 'error']
        );
    }

    /**
     * Предоставление оплаченной подписки на час без ожидания срабатывания вебхука (вызывается с js)
     *
     * @Route("/subscription-init")
     * @Method("POST")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function subscriptionInitAction(Request $request)
    {
        if (
            $request->get('plan_id') === null ||
            $request->get('checkout_id') === null ||
            $request->get('device_guid') === null
        ) {
            return new JsonResponse(['status' => false]);
        }

        $planId = $request->get('plan_id');
        $checkoutId = $request->get('checkout_id');
        $deviceGuid = $request->get('device_guid');
        $currentDateTime = new \DateTime('now');
        $expirationDateTime = (new \DateTime('now'))->modify('+1 hour');
        /** @var PlanSubscriptionRepository $subscriptionRep */
        $subscriptionRep = $this->getDoctrine()->getRepository('AppBundle:PlanSubscription');
        $currentSub = $subscriptionRep->getSubByUser($deviceGuid);

        if ($currentSub instanceof PlanSubscription) {
            if ($currentSub->getExpirationDateTime() < $currentDateTime) {
                $currentSub->setPaymentDateTime($currentDateTime)
                    ->setExpirationDateTime($expirationDateTime);
            } else {
                $currentSub->setExpirationDateTime(clone($currentSub->getExpirationDateTime())->modify('+1 hour'));
            }
            $currentSub->setPlanId($planId)
                ->setCheckoutId($checkoutId);

            $this->getDoctrine()->getManager()->flush();
        } elseif ($currentSub === null) {
            $subscription = (new PlanSubscription())->setPlanId($planId)
                ->setCheckoutId($checkoutId)
                ->setDeviceGuid($deviceGuid)
                ->setPaymentDateTime($currentDateTime)
                ->setExpirationDateTime($expirationDateTime);

            $em = $this->getDoctrine()->getManager();
            $em->persist($subscription);
            $em->flush();
        }

        return new JsonResponse(['status' => true]);
    }

    /**
     * Смена подписки юзера на указанную
     *
     * @Route("/subscription-update")
     * @Method("get")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateSubscriptionAction(Request $request)
    {
        $status = false;
        if (
            $request->get('device_guid') === null ||
            $request->get('new_plan_id') === null
        ) {
            return new JsonResponse(['status' => $status]);
        }

        /** @var PlanSubscriptionRepository $subscriptionRep */
        $subscriptionRep = $this->getDoctrine()->getRepository('AppBundle:PlanSubscription');
        $currentSub = $subscriptionRep->getSubByUser($request->get('device_guid'));

        if ($currentSub instanceof PlanSubscription) {
            $options = [
                CURLOPT_URL => 'https://vendors.paddle.com/api/2.0/subscription/users/update',
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => [
                    'vendor_id'=>$this->container->getParameter('vendor'),
                    'vendor_auth_code'=>$this->container->getParameter('vendor_auth_code'),
                    'subscription_id'=>$currentSub->getSubscriptionId(),
                    'plan_id'=>$request->get('new_plan_id'),
                ],
            ];
            /** @var PaddleApiRequest $apiRequestServ */
            $apiRequestServ = $this->get('paddle_api_request');
            $decodedResponse = $apiRequestServ->doCurlRequest($options);

            if (isset($decodedResponse['success']) && $decodedResponse['success']) {
                $nextBillDate = \DateTime::createFromFormat(
                    'Y-m-d',
                    $decodedResponse['response']['next_payment']['date']
                );
                /** @var PlanService $planServ */
                $planServ = $this->get('plan_subscription');
                $currentSub->setPlanId($request->get('new_plan_id'))
                    ->setSubscriptionId($decodedResponse['response']['subscription_id'])
                    ->setNextBillDateTime($planServ->setCurrentTimeForDate($nextBillDate));

                $this->getDoctrine()->getManager()->flush();
                $status = true;
            }
        }

        return new JsonResponse(['status' => $status]);
    }

    /**
     * Отмена подписки юзера
     *
     * @Route("/subscription-cancel")
     * @Method("get")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function unsubscribeAction(Request $request)
    {
        $status = false;
        if ($request->get('device_guid') === null) {
            return new JsonResponse(['status' => $status]);
        }

        /** @var PlanSubscriptionRepository $subscriptionRep */
        $subscriptionRep = $this->getDoctrine()->getRepository('AppBundle:PlanSubscription');
        $currentSub = $subscriptionRep->getSubByUser($request->get('device_guid'));

        if ($currentSub instanceof PlanSubscription) {
            $options = [
                CURLOPT_URL => 'https://vendors.paddle.com/api/2.0/subscription/users_cancel',
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => [
                    'vendor_id' => $this->container->getParameter('vendor'),
                    'vendor_auth_code' => $this->container->getParameter('vendor_auth_code'),
                    'subscription_id' => $currentSub->getSubscriptionId(),
                ],
            ];
            /** @var PaddleApiRequest $apiRequestServ */
            $apiRequestServ = $this->get('paddle_api_request');
            $decodedResponse = $apiRequestServ->doCurlRequest($options);

            if (isset($decodedResponse['success']) && $decodedResponse['success']) {
                $currentSub->setNextBillDateTime(null);
                $this->getDoctrine()->getManager()->flush();
                $status = true;
            }
        }

        return new JsonResponse(['status' => $status]);
    }

    /**
     * Post-запрос, которые вызывается с помощью webhook Paddle
     * обрабатывается параметр запроса alert_name, и вызывается соответствующий метод для обновления БД
     * Webhook Alert (подробнее - paddle.com/docs/reference-using-webhooks)
     *
     * @Route("/webhook")
     * @Method("POST")
     * @param Request $request
     *
     * @return Response
     */
    public function webhookAction(Request $request)
    {
        if ($request->get('alert_name') === null) {
            return (new Response())->setStatusCode(404);
        }

        try {
            /** @var PlanService $planServ */
            $planServ = $this->get('plan_subscription');
            switch ($request->get('alert_name')) {
                case 'payment_succeeded':
                    // оплата продукта
                    $statusCode = $this->paymentSuccess($request);
                    break;
                case 'subscription_created':
                    // создание подписки
                    if (
                        $request->get('subscription_plan_id') === null ||
                        $request->get('checkout_id') === null ||
                        $request->get('subscription_id') === null ||
                        $request->get('passthrough') === null ||
                        $request->get('update_url') === null ||
                        $request->get('cancel_url') === null
                    ) {
                        $statusCode = 404;
                        break;
                    }
                    $statusCode = $planServ->createSubscription
                    (
                        $request->get('subscription_plan_id'),
                        $request->get('checkout_id'),
                        $request->get('subscription_id'),
                        $request->get('passthrough'),
                        $request->get('update_url'),
                        $request->get('cancel_url')
                    );
                    break;
                case 'subscription_payment_succeeded':
                    // оплата подписки
                    if (
                        $request->get('subscription_plan_id') === null ||
                        $request->get('checkout_id') === null ||
                        $request->get('subscription_id') === null ||
                        $request->get('passthrough') === null ||
                        $request->get('next_bill_date') === null
                    ) {
                        $statusCode = 404;
                        break;
                    }
                    $statusCode = $planServ->paymentSubscription
                    (
                        $request->get('subscription_plan_id'),
                        $request->get('checkout_id'),
                        $request->get('subscription_id'),
                        $request->get('passthrough'),
                        $request->get('next_bill_date')
                    );
                    break;
                default:
                    // другой вебхук
                    $statusCode = 404;
                    break;
            }
        } catch (\Exception $e) {
            $statusCode = 500;
        }

        return (new Response())->setStatusCode($statusCode);
    }
}