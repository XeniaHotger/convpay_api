<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LicenseUser
 *
 * @ORM\Table(name="license_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LicenseUserRepository")
 */
class LicenseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="PaddleSubscription", inversedBy="licenseUsers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="paddle_subscription_id", referencedColumnName="id",onDelete="CASCADE", nullable=false)
     * })
     */
    private $paddleSubscription;

    /**
     * @var string
     *
     * @ORM\Column(name="license_id", type="string", length=255)
     */
    private $licenseId;

    /**
     * @var string
     *
     * @ORM\Column(name="device_guid", type="string", length=255)
     */
    private $deviceGuid;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get paddleSubscription
     *
     * @return PaddleSubscription
     */
    public function getPaddleSubscription(): PaddleSubscription
    {
        return $this->paddleSubscription;
    }

    /**
     * Set subscription
     *
     * @param PaddleSubscription $paddleSubscription
     *
     * @return LicenseUser
     */
    public function setPaddleSubscription(PaddleSubscription $paddleSubscription)
    {
        $this->paddleSubscription = $paddleSubscription;

        return $this;
    }

    /**
     * Set licenseId
     *
     * @param string $licenseId
     *
     * @return LicenseUser
     */
    public function setLicenseId($licenseId)
    {
        $this->licenseId = $licenseId;

        return $this;
    }

    /**
     * Get licenseId
     *
     * @return string
     */
    public function getLicenseId()
    {
        return $this->licenseId;
    }

    /**
     * Set deviceGuid
     *
     * @param string $deviceGuid
     *
     * @return LicenseUser
     */
    public function setDeviceGuid($deviceGuid)
    {
        $this->deviceGuid = $deviceGuid;

        return $this;
    }

    /**
     * Get deviceGuid
     *
     * @return string
     */
    public function getDeviceGuid()
    {
        return $this->deviceGuid;
    }
}

