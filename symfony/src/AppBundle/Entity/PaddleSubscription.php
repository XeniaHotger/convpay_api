<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * PaddleSubscription
 *
 * @ORM\Table(name="paddle_subscription")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PaddleSubscriptionRepository")
 */
class PaddleSubscription
{

    public function __construct()
    {
        $this->licenseUsers = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="license_id", type="string", length=255, unique=true)
     */
    private $licenseId;

    /**
     * @ORM\ManyToOne(targetEntity="SubscriptionInfo", inversedBy="paddleSubscriptions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subscription_id", referencedColumnName="id",onDelete="CASCADE", nullable=false)
     * })
     */
    private $subscription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="payment_date_time", type="datetime")
     */
    private $paymentDateTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiration_date_time", type="datetime")
     */
    private $expirationDateTime;

    /**
     * @ORM\OneToMany(targetEntity="LicenseUser", mappedBy="paddleSubscription")
     */
    private $licenseUsers;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set licenseId
     *
     * @param string $licenseId
     *
     * @return PaddleSubscription
     */
    public function setLicenseId($licenseId)
    {
        $this->licenseId = $licenseId;

        return $this;
    }

    /**
     * Get licenseId
     *
     * @return string
     */
    public function getLicenseId()
    {
        return $this->licenseId;
    }

    /**
     * Get subscription
     *
     * @return SubscriptionInfo
     */
    public function getSubscription(): SubscriptionInfo
    {
        return $this->subscription;
    }

    /**
     * Set subscription
     *
     * @param SubscriptionInfo $subscription
     *
     * @return PaddleSubscription
     */
    public function setSubscription(SubscriptionInfo $subscription)
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * Set paymentDateTime
     *
     * @param \DateTime $paymentDateTime
     *
     * @return PaddleSubscription
     */
    public function setPaymentDateTime($paymentDateTime)
    {
        $this->paymentDateTime = clone $paymentDateTime;

        return $this;
    }

    /**
     * Get paymentDateTime
     *
     * @return \DateTime
     */
    public function getPaymentDateTime()
    {
        return $this->paymentDateTime;
    }

    /**
     * Set expirationDateTime
     *
     * @param \DateTime $expirationDateTime
     *
     * @return PaddleSubscription
     */
    public function setExpirationDateTime($expirationDateTime)
    {
        $this->expirationDateTime = clone $expirationDateTime;

        return $this;
    }

    /**
     * Get expirationDateTime
     *
     * @return \DateTime
     */
    public function getExpirationDateTime()
    {
        return $this->expirationDateTime;
    }

    /**
     * @return Collection|LicenseUser[]
     */
    public function getLicenseUsers()
    {
        return $this->licenseUsers;
    }
}

