<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * PlanSubscription
 *
 * @ORM\Table(name="plan_subscription", uniqueConstraints={@UniqueConstraint(name="plan_device_unique", columns={"plan_id", "device_guid"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PlanSubscriptionRepository")
 */
class PlanSubscription
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="plan_id", type="integer")
     */
    private $planId;

    /**
     * @var string
     *
     * @ORM\Column(name="checkout_id", type="string", length=255, unique=true)
     */
    private $checkoutId;

    /**
     * @var string
     *
     * @ORM\Column(name="subscription_id", type="string", length=255, unique=true, nullable=true)
     */
    private $subscriptionId;

    /**
     * @var string
     *
     * @ORM\Column(name="device_guid", type="string", length=255)
     */
    private $deviceGuid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="payment_date_time", type="datetime")
     */
    private $paymentDateTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiration_date_time", type="datetime")
     */
    private $expirationDateTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="next_bill_date_time", type="datetime", nullable=true)
     */
    private $nextBillDateTime;

    /**
     * @var string
     *
     * @ORM\Column(name="update_url", type="string", length=1020, nullable=true)
     */
    private $updateUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="cancel_url", type="string", length=1020, nullable=true)
     */
    private $cancelUrl;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set planId
     *
     * @param integer $planId
     *
     * @return PlanSubscription
     */
    public function setPlanId($planId)
    {
        $this->planId = $planId;

        return $this;
    }

    /**
     * Get planId
     *
     * @return int
     */
    public function getPlanId()
    {
        return $this->planId;
    }

    /**
     * Set checkoutId
     *
     * @param string $checkoutId
     *
     * @return PlanSubscription
     */
    public function setCheckoutId($checkoutId)
    {
        $this->checkoutId = $checkoutId;

        return $this;
    }

    /**
     * Get checkoutId
     *
     * @return string
     */
    public function getCheckoutId()
    {
        return $this->checkoutId;
    }

    /**
     * Set subscriptionId
     *
     * @param string $subscriptionId
     *
     * @return PlanSubscription
     */
    public function setSubscriptionId($subscriptionId)
    {
        $this->subscriptionId = $subscriptionId;

        return $this;
    }

    /**
     * Get subscriptionId
     *
     * @return string
     */
    public function getSubscriptionId()
    {
        return $this->subscriptionId;
    }

    /**
     * Set deviceGuid
     *
     * @param string $deviceGuid
     *
     * @return PlanSubscription
     */
    public function setDeviceGuid($deviceGuid)
    {
        $this->deviceGuid = $deviceGuid;

        return $this;
    }

    /**
     * Get deviceGuid
     *
     * @return string
     */
    public function getDeviceGuid()
    {
        return $this->deviceGuid;
    }

    /**
     * Set paymentDateTime
     *
     * @param \DateTime $paymentDateTime
     *
     * @return PlanSubscription
     */
    public function setPaymentDateTime($paymentDateTime)
    {
        $this->paymentDateTime = $paymentDateTime;

        return $this;
    }

    /**
     * Get paymentDateTime
     *
     * @return \DateTime
     */
    public function getPaymentDateTime()
    {
        return $this->paymentDateTime;
    }

    /**
     * Set expirationDateTime
     *
     * @param \DateTime $expirationDateTime
     *
     * @return PlanSubscription
     */
    public function setExpirationDateTime($expirationDateTime)
    {
        $this->expirationDateTime = $expirationDateTime;

        return $this;
    }

    /**
     * Get expirationDateTime
     *
     * @return \DateTime
     */
    public function getExpirationDateTime()
    {
        return $this->expirationDateTime;
    }

    /**
     * Set nextBillDateTime
     *
     * @param \DateTime $nextBillDateTime
     *
     * @return PlanSubscription
     */
    public function setNextBillDateTime($nextBillDateTime)
    {
        $this->nextBillDateTime = $nextBillDateTime;

        return $this;
    }

    /**
     * Get nextBillDateTime
     *
     * @return \DateTime
     */
    public function getNextBillDateTime()
    {
        return $this->nextBillDateTime;
    }

    /**
     * Set updateUrl
     *
     * @param string $updateUrl
     *
     * @return PlanSubscription
     */
    public function setUpdateUrl($updateUrl)
    {
        $this->updateUrl = $updateUrl;

        return $this;
    }

    /**
     * Get updateUrl
     *
     * @return string
     */
    public function getUpdateUrl()
    {
        return $this->updateUrl;
    }

    /**
     * Set cancelUrl
     *
     * @param string $cancelUrl
     *
     * @return PlanSubscription
     */
    public function setCancelUrl($cancelUrl)
    {
        $this->cancelUrl = $cancelUrl;

        return $this;
    }

    /**
     * Get cancelUrl
     *
     * @return string
     */
    public function getCancelUrl()
    {
        return $this->cancelUrl;
    }
}

