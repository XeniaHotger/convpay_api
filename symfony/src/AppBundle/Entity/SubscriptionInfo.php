<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * SubscriptionInfo
 *
 * @ORM\Table(name="subscription_info")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubscriptionInfoRepository")
 */
class SubscriptionInfo
{

    public function __construct()
    {
        $this->paddleSubscriptions = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sub_id", type="string", length=15, unique=true)
     */
    private $subId;

    /**
     * @var string
     *
     * @ORM\Column(name="sub_name", type="string", length=255)
     */
    private $subName;

    /**
     * @var integer
     *
     * @ORM\Column(name="sub_duration", type="integer")
     */
    private $subDuration;

    /**
     * @ORM\OneToMany(targetEntity="PaddleSubscription", mappedBy="subscription")
     */
    private $paddleSubscriptions;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subId
     *
     * @param string $subId
     *
     * @return SubscriptionInfo
     */
    public function setSubId($subId)
    {
        $this->subId = $subId;

        return $this;
    }

    /**
     * Get subId
     *
     * @return string
     */
    public function getSubId()
    {
        return $this->subId;
    }

    /**
     * @return Collection|PaddleSubscription[]
     */
    public function getPaddleSubscriptions()
    {
        return $this->paddleSubscriptions;
    }

    /**
     * Set subName
     *
     * @param string $subName
     *
     * @return SubscriptionInfo
     */
    public function setSubName($subName)
    {
        $this->subName = $subName;

        return $this;
    }

    /**
     * Get subName
     *
     * @return string
     */
    public function getSubName()
    {
        return $this->subName;
    }

    /**
     * Set subDuration
     *
     * @param integer $subDuration
     *
     * @return SubscriptionInfo
     */
    public function setSubDuration($subDuration)
    {
        $this->subDuration = $subDuration;

        return $this;
    }

    /**
     * Get subDuration
     *
     * @return integer
     */
    public function getSubDuration()
    {
        return $this->subDuration;
    }
}

