<?php

namespace AppBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;

/**
 * LicenseUserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LicenseUserRepository extends EntityRepository
{
    /** @var EntityManager $em */
    protected $em;

    /** Gets licenseId-s by deviceGuid
     * @param string $deviceGuid
     * @return array
     */
    public function getLicensesByDevice($deviceGuid)
    {
        $em = $this->getEntityManager();
        $licenseIds = $em->createQueryBuilder()
            ->from('\AppBundle\Entity\LicenseUser', 'lu')
            ->select('lu.licenseId')
            ->where('lu.deviceGuid = :deviceGuid')
            ->setParameter('deviceGuid', $deviceGuid);

        return $licenseIds->getQuery()->getResult();
    }

    /** Gets deviceGuids by licenseId
     * @param string $licenseId
     * @return array
     */
    public function getDevicesByLicense($licenseId)
    {
        $em = $this->getEntityManager();
        $licenseIds = $em->createQueryBuilder()
            ->from('\AppBundle\Entity\LicenseUser', 'lu')
            ->select('lu.deviceGuid')
            ->where('lu.licenseId = :licenseId')
            ->setParameter('licenseId', $licenseId);

        return $licenseIds->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /** Gets licenseIds by deviceGuids & subId
     * @param string $deviceGuid
     * @param string $subId
     * @return array
     */
    public function getAllLicenseIdsByDeviceAndSub($deviceGuid, $subId)
    {
        $em = $this->getEntityManager();
        $sub = $em->createQueryBuilder()
            ->from('\AppBundle\Entity\PaddleSubscription', 'ps')
            ->select('lu')
            ->join('\AppBundle\Entity\LicenseUser', 'lu')
            ->where('ps.subscription = :subId')
            ->andWhere('ps.id = lu.paddleSubscription')
            ->andWhere('lu.deviceGuid = :deviceGuid')
            ->setParameter('subId', $subId)
            ->setParameter('deviceGuid', $deviceGuid);

        return $sub->getQuery()->getResult();
    }

    /** Gets licenseIds by licenseId
     * @param string $licenseId
     * @return array
     */
    public function getAllLicensesByLicenseId($licenseId) {
        $em = $this->getEntityManager();
        $sub = $em->createQueryBuilder()
            ->from('\AppBundle\Entity\LicenseUser', 'lu')
            ->select('lu')
            ->where('lu.licenseId = :licenseId')
            ->setParameter('licenseId', $licenseId);

        return $sub->getQuery()->getResult();
    }
}
