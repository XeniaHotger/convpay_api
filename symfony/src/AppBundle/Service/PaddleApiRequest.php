<?php

namespace AppBundle\Service;

class PaddleApiRequest
{
    /**
     * Curl запрос
     *
     * @param array $options
     *
     * @return array | bool
     */
    public function doCurlRequest($options)
    {
        $basicOptions = [
            CURLOPT_TIMEOUT => 5,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => true,
        ];
        $ch = curl_init();
        curl_setopt_array($ch, $basicOptions + $options);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response && strlen(trim($response)) !== 0 ?
            json_decode($response, true) :
            false;
    }
}