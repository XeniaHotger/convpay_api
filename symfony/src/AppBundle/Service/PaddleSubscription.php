<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;

use AppBundle\Entity\PaddleSubscription as PaddleSubscriptionEntity;
use AppBundle\Entity\SubscriptionInfo as SubscriptionInfoEntity;
use AppBundle\Entity\LicenseUser as LicenseUserEntity;
use AppBundle\Repository\LicenseUserRepository;
use AppBundle\Repository\SubscriptionInfoRepository;

class PaddleSubscription
{
    const USERS_PER_LICENSE = 8;

    /** @var EntityManager $em */
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct($em)
    {
        $this->em = $em;
    }

    /**
     * @param PaddleSubscriptionEntity $currentSub
     * @param string $licenseId
     * @param \DateTime $expirationDateTime
     */
    public function updateExistingSubscription($currentSub, $licenseId, $expirationDateTime)
    {
        // if the subscription has already expired then create new payment DateTime
        if ($currentSub->getExpirationDateTime() < new \DateTime('now')) {
            $currentSub->setPaymentDateTime(new \DateTime('now'));
        }
        $currentSub->setExpirationDateTime($expirationDateTime);
        $currentSub->setLicenseId($licenseId);

        $this->em->flush();
    }

    /**
     * @param SubscriptionInfoEntity $sub
     * @param string $licenseId
     * @param \DateTime $expirationDateTime
     *
     * @return \AppBundle\Entity\PaddleSubscription
     */
    public function addNewSubscription($sub, $licenseId, $expirationDateTime)
    {
        $subscription = (new PaddleSubscriptionEntity())
            ->setSubscription($sub)
            ->setPaymentDateTime(new \DateTime('now'))
            ->setExpirationDateTime($expirationDateTime)
            ->setLicenseId($licenseId);

        $this->em->persist($subscription);
        $this->em->flush();

        return $subscription;
    }

    /**
     * @param string $deviceGuid
     * @param string $licenseId
     * @param PaddleSubscriptionEntity $sub
     */
    public function addDeviceGuid($deviceGuid, $licenseId, $sub)
    {
        $licenseUser = (new LicenseUserEntity())
            ->setPaddleSubscription($sub)
            ->setLicenseId($licenseId)
            ->setDeviceGuid($deviceGuid);

        $this->em->persist($licenseUser);
        $this->em->flush();
    }

    /**
     * @param array $allCurrentLicenses
     * @param string $licenseId
     */
    public function updateAllLicenses($allCurrentLicenses, $licenseId)
    {
        foreach ($allCurrentLicenses as $currentLicense) {
            $currentLicense->setLicenseId($licenseId);
        }

        $this->em->flush();
    }

    /**
     * Получение entity пользователя, если у него ранее уже были покупки
     *
     * @param string $deviceGuid
     * @param string $subId
     *
     * @return PaddleSubscription | null
     */
    public function getCurrentSub($deviceGuid, $subId)
    {
        /** @var SubscriptionInfoRepository $subscriptionInfoRep */
        $subscriptionInfoRep = $this->em->getRepository('AppBundle:SubscriptionInfo');
        /** @var LicenseUserRepository $licenseUserRep */
        $licenseUserRep = $this->em->getRepository('AppBundle:LicenseUser');
        $allCurrentLicenses = $licenseUserRep->getAllLicenseIdsByDeviceAndSub($deviceGuid, $subscriptionInfoRep->getSubKeyById($subId));
        $currentSub = null;

        // if there is no old licenseId, then there is no old subId
        if (count($allCurrentLicenses) > 0 && $allCurrentLicenses[0] instanceof LicenseUserEntity) {
            $firstCurrentLicense = $allCurrentLicenses[0];
            // get old licenseId, get currentSub by licenseId
            $currentSub = $firstCurrentLicense->getPaddleSubscription();
        }

        return $currentSub;
    }

    /**
     * Добавление в базу новой записи о подписке юзера при успешном завершении оплаты
     *
     * @param string $licenseId
     * @param string $deviceGuid
     * @param string $subId
     * @param \DateTime $expirationDateTime
     *
     * @return bool
     */
    public function addToDatabase($licenseId, $deviceGuid, $subId, $expirationDateTime)
    {
        try {
            /** @var SubscriptionInfoRepository $subscriptionInfoRep */
            $subscriptionInfoRep = $this->em->getRepository('AppBundle:SubscriptionInfo');
            $currentSubProduct = $subscriptionInfoRep->findOneBy(['subId' => $subId]);
            // add new subscription
            $currentSub = $this->addNewSubscription($currentSubProduct, $licenseId, $expirationDateTime);
            // insert in LicenseUsers
            $this->addDeviceGuidIfNotExisted($deviceGuid, $licenseId, $currentSub);

            return true;
        }
        catch(\Exception $e) {
            return false;
        }
    }

    /**
     * Продление подписки при успешном завершении оплаты
     *
     * @param string $licenseId
     * @param string $deviceGuid
     * @param PaddleSubscriptionEntity $currentSub
     * @param \DateTime $expirationDateTime
     *
     * @return bool
     */
    public function updateDatabase($licenseId, $deviceGuid, $currentSub, $expirationDateTime)
    {
        try {
            /** @var LicenseUserRepository $licenseUserRep */
            $licenseUserRep = $this->em->getRepository('AppBundle:LicenseUser');
            $allCurrentLicenses = $licenseUserRep->getAllLicensesByLicenseId($currentSub->getLicenseId());
            // update existing subscription - increase the ExpirationDateTime
            $this->updateExistingSubscription($currentSub, $licenseId, $expirationDateTime);
            // update licenseId values for all deviceGuids in LicenseUsers
            $this->updateAllLicenses($allCurrentLicenses, $licenseId);
            // insert in LicenseUsers
            $this->addDeviceGuidIfNotExisted($deviceGuid, $licenseId, $currentSub);

            return true;
        }
        catch(\Exception $e) {
            return false;
        }
    }

    /**
     * Check licenseId and deviceGuid row && if there is no deviceGuid, add it
     *
     * @param string $deviceGuid
     * @param string $licenseId
     * @param PaddleSubscriptionEntity $sub
     *
     * @return bool
     */
    public function addDeviceGuidIfNotExisted($deviceGuid, $licenseId, $sub)
    {
        /** @var LicenseUserRepository $licenseUserRep */
        $licenseUserRep = $this->em->getRepository('AppBundle:LicenseUser');
        $deviceGuids = $licenseUserRep->getDevicesByLicense($licenseId);
        $deviceGuidsList = array_column($deviceGuids, 'deviceGuid');

        if (!in_array($deviceGuid, $deviceGuidsList)) {
            if (count($deviceGuids) < self::USERS_PER_LICENSE) {
                $this->addDeviceGuid($deviceGuid, $licenseId, $sub);
            } else {
                return false;
            }
        }
        return true;
    }
}