<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;

use AppBundle\Entity\PlanSubscription as PlanSubscriptionEntity;
use AppBundle\Repository\PlanSubscriptionRepository;

class PlanSubscription
{
    /** @var EntityManager $em */
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct($em)
    {
        $this->em = $em;
    }

    /**
     * @param \DateTime $date
     *
     * @return \DateTime $date
     */
    public function setCurrentTimeForDate($date)
    {
        $date = $date->setTime(date('H'), date('i'), date('s'));

        return $date;
    }

    /**
     * Создание новой подписки юзера (вебхук)
     * Создает запись в базе, записывая update_url и cancel_url
     * Запись активна в течении получаса, для ожидания подтверждения оплаты (subscription_payment_succeeded)
     *
     * @param string $planId
     * @param string $checkoutId
     * @param string $subscriptionId
     * @param string $deviceGuid
     * @param string $updateUrl
     * @param string $cancelUrl
     *
     * @return integer
     */
    public function createSubscription($planId, $checkoutId, $subscriptionId, $deviceGuid, $updateUrl, $cancelUrl)
    {
        $currentDateTime = new \DateTime('now');
        /** @var PlanSubscriptionRepository $subscriptionRep */
        $subscriptionRep = $this->em->getRepository('AppBundle:PlanSubscription');
        $currentSub = $subscriptionRep->getSubByUser($deviceGuid);

        if ($currentSub instanceof PlanSubscriptionEntity) {
            $currentSub->setCheckoutId($checkoutId)
                ->setSubscriptionId($subscriptionId)
                ->setUpdateUrl($updateUrl)
                ->setCancelUrl($cancelUrl);
        } elseif ($currentSub === null) {
            $subscription = (new PlanSubscriptionEntity())->setPlanId($planId)
                ->setCheckoutId($checkoutId)
                ->setSubscriptionId($subscriptionId)
                ->setDeviceGuid($deviceGuid)
                ->setPaymentDateTime($currentDateTime)
                ->setExpirationDateTime($currentDateTime)
                ->setUpdateUrl($updateUrl)
                ->setCancelUrl($cancelUrl);

            $this->em->persist($subscription);
        }
        $this->em->flush();

        return 200;
    }

    /**
     * Добавление в базу оплаченной подписки (вебхук)
     *
     * @param string $planId
     * @param string $checkoutId
     * @param string $subscriptionId
     * @param string $deviceGuid
     * @param \DateTime $nextBillDate
     *
     * @return integer
     */
    public function paymentSubscription($planId, $checkoutId, $subscriptionId, $deviceGuid, $nextBillDate)
    {
        $nextBillDateTime = $this->setCurrentTimeForDate($nextBillDate);
        $currentDateTime = new \DateTime('now');
        /** @var PlanSubscriptionRepository $subscriptionRep */
        $subscriptionRep = $this->em->getRepository('AppBundle:PlanSubscription');
        $currentSub = $subscriptionRep->getSubByUser($deviceGuid);

        if ($currentSub instanceof PlanSubscriptionEntity) {
            if ($currentSub->getExpirationDateTime() < $currentDateTime) {
                $currentSub->setPaymentDateTime($currentDateTime);
            }
            $currentSub->setPlanId($planId)
                ->setCheckoutId($checkoutId)
                ->setSubscriptionId($subscriptionId)
                ->setExpirationDateTime($nextBillDateTime)
                ->setNextBillDateTime($nextBillDateTime);
        } elseif ($currentSub === null) {
            $subscription = (new PlanSubscriptionEntity())->setPlanId($planId)
                ->setCheckoutId($checkoutId)
                ->setSubscriptionId($subscriptionId)
                ->setDeviceGuid($deviceGuid)
                ->setPaymentDateTime($currentDateTime)
                ->setExpirationDateTime($nextBillDateTime)
                ->setNextBillDateTime($nextBillDateTime);

            $this->em->persist($subscription);
        }
        $this->em->flush();

        return 200;
    }
}